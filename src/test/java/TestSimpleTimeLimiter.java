import com.google.common.util.concurrent.SimpleTimeLimiter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TestSimpleTimeLimiter {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        SimpleTimeLimiter stl = new SimpleTimeLimiter(executorService);
        String result = null;
        try {
            result = stl.callWithTimeout(() -> {
                Thread.sleep(5000);
                System.out.println("呵呵");
                return "result";
            }, 4, TimeUnit.SECONDS, false);
            System.out.println("正常调用：" + result);
        } catch (Exception e) {
            System.out.println("超时调用：" + result);
            e.printStackTrace();
        } finally {
            executorService.shutdown();
        }
    }
}
